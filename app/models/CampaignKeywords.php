<?php
use Phalcon\Mvc\Model\Validator\Uniqueness,
	Phalcon\Mvc\Model\Validator\PresenceOf;

/**
 *This class is used to fetch campaign_keywords collection from database.
 */
class CampaignKeywords extends \Phalcon\Mvc\Collection
{
	/**
	 *This function is used to fetch campaign_keywords collection from database.
	 */
	public function getSource()
    {
        return "campaign_keywords";
    }
}
<?php
/**
 *This class is used to fetch search_result_log collection from database.
 */
class SearchResultLog extends \Phalcon\Mvc\Collection
{
    public function getSource()
    {
        return "search_result_log";
    }
	
}
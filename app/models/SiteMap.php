<?php

/**
 *This class is used to fetch track_visitors collection from database.
 */
class SiteMap extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "site_xmlmap";
    }
}
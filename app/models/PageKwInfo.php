<?php
/**
 *This class is used to fetch page_kw_info collection from database.
 */
class PageKwInfo extends \Phalcon\Mvc\Collection
{
	/**
	 *This function is used to fetch page_kw_info collection from database.
	 */
	public function getSource()
    {
        return "page_kw_info";
    }
	
}
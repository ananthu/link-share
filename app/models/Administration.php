<?php
/**
 *This class is used to fetch users collection from database.
 */
class Administration extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "users";
    }
}
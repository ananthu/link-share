<?php
/**
 *This class is used to fetch page_link_info collection from database.
 */
class PageLinkInfo extends \Phalcon\Mvc\Model
{
	/**
	 *This method is used to fetch page_link_info collection from database.
	 */
	public function getSource()
    {
        return "page_link_info";
    }
	
}
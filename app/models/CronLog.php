<?php
/**
 *This class is used to fetch cown_log collection from database.
 */
class CronLog extends \Phalcon\Mvc\Collection
{
	/**
	 *This function is used to fetch cown_log collection from database which stores log details of crons.
	 */
	public function getSource()
    {
        return "crown_log";
    }
	
}
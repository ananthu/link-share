<?php
ananthu
// controls for administrative page
use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;
/**
 *This class is used to provide administratve option to the site.
 */
class AdministrationController extends \Phalcon\Mvc\Controller
{
	/**
	 *This function is used to initialize the page for administrative page.
	 */
   public function initialize()
    {
       $this->view->setVar("page", "administration");
    }
    
    /**
    *This function is used to initialize the fetch users from database.
    */
    public function indexAction()
    {
        $get_users = Administration::find();
		$this->view->setVar('get_users', $get_users);
    }
    public function showAction($postId)
    {

    }
    
    /**
     *This function is used to allow for ser data to be saved within database.
     */
	public function save_usersAction()
	{
		$request = $this->request;
        if ($request->isPost()) 
		{
			$username = $request->getPost('username');
			$password = $request->getPost('password');
			
			$administration = new Administration();
			$administration->username = $username;
                        $administration->type = "normal";
			$administration->password = md5($password);
			
			if ($administration->save() == false) {
				foreach ($administration->getMessages() as $message) {
					echo $message->getMessage().'<br>';
				}
			}
			else
				$this->flash->success('The user is saved successfully.');
		}
		$get_users = Administration::find();
		$this->view->setVar('get_users', $get_users);
	}
	
	/**
	 *This function is used to delete uer from database (admin is prohibited).
	 */
	public function delete_userAction($postid)
	{
		$user = Administration::findFirst("id = '$postid'");
                $msg = "";
		if (!empty($user)) {
			if ($user->delete() == false) {
				$msg = "Sorry, we can't delete the user right now: \n";
				foreach ($user->getMessages() as $message) {
					echo $message, "\n";
				}
			} else {
				$msg = "The user ".$user->username." was deleted successfully!";
			}
		}
		else
                {
                    $msg = "Sorry, we can't find this user. \n";
                }
        echo json_encode(array("deleted" =>"sucss", "msg" => $msg));
        exit;
	}
	
	/**
	 *This function is used to edit user information.
	*/
	public function edit_userAction($postid)
	{
		$user = Administration::findFirst("id = '$postid'");
		if (!empty($user)) {
			
			$request = $this->request;
			if ($request->isPost()) 
			{
				$password = $request->getPost('new_password');
				$update_user = Administration::findFirst($arr);
				$update_user->password = md5($password);
				if ($update_user->save() == false) {
					echo "Sorry, we can't update the user's password right now: \n";
					foreach ($user->getMessages() as $message) {
						echo $message, "\n";
					}
				} else {
					echo ucfirst($user->username)."'s password was updated successfully!";
				}
			}
			$this->view->setVar('user_details', $user);
		}
		else
			echo "Sorry, we can't find this user. \n";
		
		$get_users = Administration::find();
		$this->view->setVar('get_users', $get_users);
	}

}
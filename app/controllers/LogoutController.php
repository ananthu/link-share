<?php
/**
 *This class is used to provide link to Logout from the site.
 */
use Phalcon\Mvc\Controller,
    Phalcon\Mvc\View;

class LogoutController extends \Phalcon\Mvc\Controller
{
	/**
	 *This function is used to call function to initialize the variables that may in use.
	 */
    public function initialize()
    {
    	
    }
    
    /**
     *This function is used to destroy the user's sessions.
    */
    public function indexAction()
    {
        $response = new Phalcon\Http\Response();
        $this->session->destroy();
        
        //An HTTP Redirect
        $this->response->redirect('index/index');

        //Disable the view to avoid rendering
        $this->view->disable();
    }
}

<?php
try {
    //Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(array(
        '../app/controllers/',
        '../app/models/'
    ))->register();
    
    //Setting up the view component
    //Create a DI
    $di = new Phalcon\DI\FactoryDefault();
    $di->set('view', function(){
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    // Global Initialization of Phalcon Sessions
    $di->setShared('session', function() {
        $session = new Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    });
    
    
    //Setup the database service
    $di->set('db', function(){
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            "host" => "95.85.63.246",
            "dbname" => "site_analysis",
            "port" => 3306,
            "username" => "root",
            "password" => "c0deddes1gn"
        ));
    });
    //Setting up the collection component   
    $di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
    }, true);
    
    $di->set('modelsManager', function() {
      return new Phalcon\Mvc\Model\Manager();
    });

    $application = new \Phalcon\Mvc\Application($di);
    //Handle the request
    echo $application->handle()->getContent(); 

    // Get output contents
}
// Fired when there is any error or warning occur during the program work.
catch(\Phalcon\Exception $e) {
     echo "PhalconException: ", $e->getMessage();
}



